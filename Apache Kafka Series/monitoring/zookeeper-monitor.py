#######################################################################
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#######################################################################

from xml.dom.minidom import getDOMImplementation
import telnetlib
import socket

PRTG_XML_RESULT_TAG = 'prtg'
ZK_PORT = 2181
ZK_HOST = 'localhost'

DOM = getDOMImplementation()
XML = DOM.createDocument(None, PRTG_XML_RESULT_TAG, None)


def create_prtg_result_node(channel_name, value):

    # <result>
    #   <channel>{channel_name}</channel>
    #   <showChart>1</showChart>
    #   <showTable>1</showTable>
    #   <value>{value}</value>
    # </result >

    node = XML.createElement('result')

    channel_tag = XML.createElement('channel')
    channel_data = XML.createTextNode(channel_name)
    channel_tag.appendChild(channel_data)

    show_chart_tag = XML.createElement('showChart')
    show_chart_data = XML.createTextNode('1')
    show_chart_tag.appendChild(show_chart_data)

    show_table_tag = XML.createElement('showTable')
    show_table_data = XML.createTextNode('1')
    show_table_tag.appendChild(show_table_data)

    value_tag = XML.createElement('value')
    value_data = XML.createTextNode(str(value))
    value_tag.appendChild(value_data)

    node.appendChild(channel_tag)
    node.appendChild(show_chart_tag)
    node.appendChild(show_table_tag)
    node.appendChild(value_tag)

    return node

if __name__ == '__main__':
    top_element = XML.documentElement

    # ruok
    try:
        tn = telnetlib.Telnet(ZK_HOST, ZK_PORT)
        tn.write('ruok')
        imok = 1 if (tn.read_all() == 'imok') else 0 
    except socket.timeout:
        imok = 0
    top_element.appendChild(create_prtg_result_node('zk_are_you_ok', imok))

    # mntr
    try:
        tn = telnetlib.Telnet(ZK_HOST, ZK_PORT)
        tn.write('mntr')
        mntr = tn.read_all().strip()
    except socket.timeout:
        mntr = None

    for line in mntr.split('\n'):
        key, value = line.split('\t')
	if key == 'zk_server_state':
		if value == 'follower':
			value = 2
		elif value == 'leader':
			value = 1
		else:
			value = 0
        if not key == 'zk_version':
            top_element.appendChild(create_prtg_result_node(key, value))

    pretty_xml_as_string = XML.toprettyxml()
    print pretty_xml_as_string

